﻿
namespace Frank.Actions
{
    /// <summary>
    /// Authentication that authenticates everybody.
    /// </summary>
    public class EverybodyAuthentication : IAuthentication
    {
        /// <inheritdoc/>
        public bool Authenticate()
        {
            return true;
        }
    }
}
