﻿/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Frank.Actions
{
    /// <summary>
    /// Interface for authentication objects.
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// Authenticate a user (e.g. ask for a passphrase)
        /// </summary>
        /// <returns><code>true</code> if authentication was correct, otherwise <code>false</code>.</returns>
        bool Authenticate();
    }
}
