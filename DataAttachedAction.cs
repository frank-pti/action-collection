/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;

namespace Frank.Actions
{
    /// <summary>
    /// Action with custom data attached to it.
    /// </summary>
    public class DataAttachedAction<T>: TranslatableAction
    {
        /// <summary>
        /// Delegate method for activated event.
        /// </summary>
        public delegate void ActivatedCallback(DataAttachedAction<T> sender);
        /// <summary>
        /// Occurs when activated.
        /// </summary>
        public new event ActivatedCallback Activated;

        private T data;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Actions.DataAttachedAction`1"/> class.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="name">Name.</param>
        /// <param name="translatableLabel">Translatable label.</param>
        public DataAttachedAction (T data, string name, TranslationString translatableLabel):
            this(data, name, translatableLabel, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Actions.DataAttachedAction`1"/> class.
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="name">Name.</param>
        /// <param name="translatableLabel">Translatable label.</param>
        /// <param name="toolTip">Tool tip.</param>
        public DataAttachedAction (T data, string name, TranslationString translatableLabel, TranslationString toolTip):
            base(name, translatableLabel, toolTip)
        {
            this.data = data;
        }

        /// <summary>
        /// Gets the attached data.
        /// </summary>
        /// <value>The attached data.</value>
        public T AttachedData {
            get {
                return this.data;
            }
        }

        /// <summary>
        /// Raises the activated event.
        /// </summary>
        protected override void OnActivated ()
        {
            if (Activated != null) {
                Activated(this);
            }
        }
    }
}

