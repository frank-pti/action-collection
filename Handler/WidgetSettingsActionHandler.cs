/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using InternationalizationUi;
using Logging;
using System;
using System.Reflection;

namespace Frank.Actions.Handler
{
    /// <summary>
    /// Base class for actions that manipulate a configuration object (usually configuration dialogs)
    /// </summary>
    /// <typeparam name="T">The configuration type.</typeparam>
    public abstract class WidgetSettingsActionHandler<T> : AuthenticationActionHandler
        where T : class
    {
        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="configuration">The configuration that is manipulated.</param>
        /// <param name="authentication">The authentication.</param>
        public WidgetSettingsActionHandler(TranslatableAction action, T configuration, IAuthentication authentication) :
            base(action, authentication)
        {
            Configuration = configuration;
        }

        protected void HandleWidgetSettingsChanged(object sender, PropertyInfo property)
        {
            Type configPropertiesType = Configuration.GetType();
            PropertyInfo[] configProperties = configPropertiesType.GetProperties();
            foreach (PropertyInfo info in configProperties) {
                try {
                    if (info.Name == property.Name) {
                        MethodInfo infoSetter = info.GetSetMethod();
                        MethodInfo propertyGetter = property.GetGetMethod();
                        object parameter = propertyGetter.Invoke(sender, null);
                        infoSetter.Invoke(Configuration, new object[] { parameter });
                    }
                } catch (Exception e) {
                    Logger.Log(e, "Error while handling property change");
                    if (e.InnerException != null) {
                        Logger.Log(e.InnerException, "Inner exception");
                    }
                }
            }
        }

        protected T Configuration
        {
            get;
            private set;
        }
    }
}

