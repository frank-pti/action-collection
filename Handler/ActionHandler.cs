using InternationalizationUi;
using Logging;
/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Actions.Handler
{
    /// <summary>
    /// Base class for action handlers.
    /// </summary>
    public abstract class ActionHandler : IDisposable
    {
        /// <summary>
        /// Delegate method for exception caught event.
        /// </summary>
        public delegate void ExceptionCaughtDelegate(Exception exception);
        /// <summary>
        /// Occurs when exception caught.
        /// </summary>
        public event ExceptionCaughtDelegate ExceptionCaught;

        private TranslatableAction action;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Actions.Handler.ActionHandler"/> class.
        /// </summary>
        /// <param name="action">Action.</param>
        public ActionHandler(TranslatableAction action)
        {
            this.action = action;
            this.action.Activated += HandleActionActivated;
        }

        private void HandleActionActivated(object sender, EventArgs args)
        {
            try {
                HandleAction();
            } catch (Exception e) {
                Logger.Log(e, "Error while activating action");
                if (ExceptionCaught != null) {
                    ExceptionCaught(e);
                }
            }
        }

        /// <summary>
        /// Handles the action.
        /// </summary>
        protected abstract void HandleAction();

        #region IDisposable implementation
        /// <summary>
        /// Releases all resource used by the <see cref="ActionHandler"/> object.
        /// </summary>
        /// <remarks>Call <see cref="Dispose"/> when you are finished using the
        /// <see cref="ActionHandler"/>. The <see cref="Dispose"/> method leaves the
        /// <see cref="ActionHandler"/> in an unusable state. After calling <see cref="Dispose"/>,
        /// you must release all references to the <see cref="ActionHandler"/> so the garbage
        /// collector can reclaim the memory that the <see cref="ActionHandler"/> was occupying.</remarks>
        public void Dispose()
        {
            action.Activated -= HandleActionActivated;
            action = null;
        }
        #endregion
    }
}

