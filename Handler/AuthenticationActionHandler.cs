/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using InternationalizationUi;

namespace Frank.Actions.Handler
{
    /// <summary>
    /// Base class for actions that need to be authenticated before they can be performed,
    /// (e.g. changing of program settings).
    /// </summary>
    public abstract class AuthenticationActionHandler : DialogActionHandler
    {
        private IAuthentication authentication;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="action">Action.</param>
        /// <param name="authentication">The authentication.</param>
        public AuthenticationActionHandler(TranslatableAction action, IAuthentication authentication) :
            base(action)
        {
            this.authentication = authentication;
        }

        #region implemented abstract members of ProbeNet.Mini.Actions.Handler.ActionHandler
        protected override sealed void HandleAction()
        {
            if (authentication.Authenticate()) {
                HandleAuthenticatedAction();
            }
        }
        #endregion

        #region implemented abstract members of Frank.Actions.Handler.DialogActionHandler
        protected override TranslatableIconButton DefaultCloseButton
        {
            get
            {
                return null;
            }
        }
        #endregion

        /// <summary>
        /// Handle method for the authenticated action.
        /// This method is called only after successful authentification.
        /// </summary>
        protected abstract void HandleAuthenticatedAction();
    }
}

