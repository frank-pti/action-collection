/*
 * An action handling library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets;
using Internationalization;
using InternationalizationUi;
using System;

namespace Frank.Actions.Handler
{
    /// <summary>
    /// Base class for actions that show a dialog. It provides a protect method for creating and showing
    /// a dialog.
    /// </summary>
    public abstract class DialogActionHandler : ActionHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Actions.Handler.DialogActionHandler"/> class.
        /// </summary>
        /// <param name="action">Action.</param>
        public DialogActionHandler(TranslatableAction action) :
            base(action)
        {
        }

        /// <summary>
        /// Gets the default close button.
        /// </summary>
        /// <value>The default close button.</value>
        protected abstract TranslatableIconButton DefaultCloseButton
        {
            get;
        }

        /// <summary>
        /// Creates the and show dialog.
        /// </summary>
        /// <returns>The and show dialog.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="dialogParent">Dialog parent.</param>
        /// <param name="name">Name.</param>
        /// <param name="dialogContentWidget">Dialog content widget.</param>
        /// <param name="dialogIcon">Dialog icon</param>
        /// <param name="responseHandler">Response handler.</param>
        /// <param name="buttons">Buttons.</param>
        protected Gtk.Dialog CreateAndShowDialog(
            I18n i18n,
            Gtk.Window dialogParent,
            TranslationString name,
            IContentWidget dialogContentWidget,
            Gdk.Pixbuf dialogIcon = null,
            Gtk.ResponseHandler responseHandler = null,
            Nullable<Gdk.Rectangle> sizeRequest = null,
            params object[] buttons)
        {
            if (buttons == null || buttons.Length == 0) {
                buttons = new object[] {
                    DefaultCloseButton,
                    Gtk.ResponseType.Close
                };
            }
            Gtk.Dialog dialog = new Gtk.Dialog(
                name.Tr,
                dialogParent,
                Gtk.DialogFlags.Modal);
            if (dialogIcon != null) {
                dialog.Icon = dialogIcon;
            }
            dialogContentWidget.DialogParent = dialog;
            if (sizeRequest != null) {
                dialog.SetSizeRequest(sizeRequest.Value.Width, sizeRequest.Value.Height);
            }
            for (int i = 0; i < buttons.Length; i += 2) {
                Gtk.Widget w = null;
                if (buttons[i] is TranslationString) {
                    w = new TranslatableButton(buttons[i] as TranslationString);
                } else if (buttons[i] is string) {
                    w = new Gtk.Button() {
                        Label = (string)buttons[i]
                    };
                } else if (buttons[i] is TranslatableIconButton) {
                    w = buttons[i] as Gtk.Widget;
                }
                dialog.AddActionWidget(w, (Gtk.ResponseType)buttons[i + 1]);
            }
            dialog.VBox.PackStart(dialogContentWidget.AsWidget);
            dialog.ShowAll();
            dialogContentWidget.ShowAll();
            dialog.WindowPosition = Gtk.WindowPosition.CenterOnParent;
            if (responseHandler != null) {
                dialog.Response += responseHandler;
            }
            dialog.Run();
            dialog.Destroy();
            return dialog;
        }
    }
}

